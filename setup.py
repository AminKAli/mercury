from setuptools import setup, find_packages

setup(name='mercury',
      version='0.9',
      description='Flexible Fast Entity Component System Framework',
      url='http://bitbucket.org/AminKAli/mercury',
      author='Amin Ali',
      author_email='Amin.K.Ali@gmail.com',
      license='MIT',
      packages=find_packages(),
      zip_safe=False)