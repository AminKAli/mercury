# Mercury

A fast and flexible Entity-Component-System framework for python
__________________________________________________

## What is an Entity Component System?
An Entity-component-system (ECS) is a software architecture pattern that
separates the functionality into individual components that are mostly
independent of one another. Composition is used instead of inheritance. The
entity is a general purpose object. Usually, it only consists of a unique id
and a container. The component consists of a minimal set of data needed for
a specific purpose. Systems are single purpose functions that take a set of
entities which have a specific component (or set of components) and update
them.

A typical application of ECS is in games design when there are many type of
objects with many type of attributes. Instead of using a complex inheritance
hierarchy of object types, entities are used with components.
__________________________________________________

Head on over to the [wiki](http://bitbucket.org/AminKAli/mercury/wiki/Home) for a detailed breakdown of the API and example code.


## TODO
* Write some tests
* Complete documentation
* Create Examples

## CREDIT

## LICENSE
This content is released under the MIT License (see [LICENSE](https://bitbucket.org/AminKAli/mercury/raw/master/LICENSE) file)
