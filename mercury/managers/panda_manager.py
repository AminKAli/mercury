from mercury import Manager

class PandaManager(Manager):

    def __init__(self, max_entities, task_mgr, task_chain_name,
        num_threads = None, tick_clock = None, thread_priority = None,
        frame_budget = None, frame_sync = None, timeslice_priority = None):
        super(PandaManager, self).__init__(max_entities)
        self.task_mgr = task_mgr
        self.task_mgr.setupTaskChain(task_chain_name, num_threads, tick_clock,
            thread_priority, frame_budget, frame_sync, timeslice_priority)

    def add_system(self, system):
        if super(PandaManager, self).add_system(system):
            self.task_mgr.add(system.tick, system.class_name, extraArgs=[], appendTask=True)
            return True
        else:
            return False

    def remove_system(self, system_type):
        if super(PandaManager, self).remove_system(system_type):
            self.task_mgr.remove(system_type)
            return True
        else:
            return False