class _System(object):

    def __init__(self):
        self.class_name = self.__class__.__name__

    def valid(self, components):
        pass

    def tick(self, data):
        self.update(data)

    def prepare(self):
        pass  # Overridden by subclasses

    def update(self, data):
        pass  # Overridden by subclasses

    def destroy(self):
        pass  # Overridden by subclasses