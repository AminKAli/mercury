class _Condition(object):
    conditions = {}
    def __init__(self, *args):
        self.conditions = args

    def valid(self, comp_names):
        return True

class And(_Condition):
    def valid(self, comp_names):
        for condition in self.conditions:
            if isinstance(condition, basestring):
                if condition not in comp_names:
                    return False
            else:
                if not condition.valid(comp_names):
                    return False
        return True

class Or(_Condition):
    def valid(self, comp_names):
        for condition in self.conditions:
            if isinstance(condition, basestring):
                if condition in comp_names:
                    return True
            else:
                if condition.valid(comp_names):
                    return True
        return False

class AndNot(_Condition):
    def valid(self, comp_names):
        for condition in self.conditions:
            if isinstance(condition, basestring):
                if condition in comp_names:
                    return False
            else:
                if condition.valid(comp_names):
                    return True
        return True

class OrNot(_Condition):
    def valid(self, comp_names):
        for condition in self.conditions:
            if isinstance(condition, basestring):
                if condition not in comp_names:
                    return True
            else:
                if condition.valid(comp_names):
                    return True
        return False