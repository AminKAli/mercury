from system import _System
from conditions import _Condition
class EntitySystem(_System):

    condition = _Condition()

    def __init__(self):
        self.entities = {}
        self.class_name = self.__class__.__name__

    def valid(self, components):
        comp_names = components.keys()
        if isinstance(self.condition, basestring):
            if comp_names.include(self.condition):
                return True
        else:
            if self.condition.valid(comp_names):
                return True
        return False
    def valid(self, components):
        comp_names = components.keys()
        if isinstance(self.condition, basestring):
            if comp_names.include(self.condition):
                return True
        else:
            if self.condition.valid(comp_names):
                return True
        return False

    #### Manage Entities ####
    #########################
    def add_entity(self, entity_id, components):
        if self.valid(components):
            self.entities[entity_id] = components
            return True;
        else:
            return False

    def remove_entity(self, entity_id):
        del self.entities[entity_id]

    def add_component(self, entity_id, component):
        components = self.entities[entity_id]
        components[component.cname] = component
        if self.valid(entity_id, components):
            self.entities[entity_id] = components
        else:
            del self.entities[entity_id]

    def remove_component(self, entity_id, component):
        components = self.entities[entity_id]
        components.pop(component.cname, None)
        if selfvalid(components):
            self.entities[entity_id] = components
        else:
            del self.entities[entity_id]


    #### Process Entities ####
    ##########################
    def tick(self, data):
        self.update(data)
        for entity, components in self.entities.iteritems():
            self.process(entity, components, data)

    def prepare(self):
        pass  # Overridden by subclasses

    def update(self, data):
        pass  # Overridden by subclasses

    def destroy(self):
        pass  # Overridden by subclasses

    def process(self, entity_id, components, previous_time):
        pass  # Overridden by subclasses
