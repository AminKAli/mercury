from collections import deque
from void_system import VoidSystem

class Manager(object):

    #### Manager Initialization ####
    ################################
    def __init__(self, max_entities):
        self.max_entities = max_entities  # Max Number of entities in action within the manager
        self.free_ids = deque()  # List of all entity IDs that have been released
        self.highest_id = 0  # The highest entity ID that has been used.
        self.systems = {}  # Dictionary of all Systems used
        self.entities = {}  # Dictionary of all Entities used


    #### Entity Management ####
    ###########################
    def create_entity(self, components):
        entity_id = self.next_id()
        self.entities[entity_id] = components
        for component  in components.values():
            component.entity_id = entity_id
        for system in self.systems:
            system.add_entity(entity_id, components)
        return entity_id

    def delete_entity(self, entity_id):
        if entity_id in self.entities:
            for system in self.systems:
                system.remove_entity(entity_id)
            self.freed_ids.append(entity_id)
            return True
        return False

    def add_component(self, entity_id, component):
        if entity_id in self.entities:
            component.entity_id = entity_id
            for system in self.systems:
                system.add_component(entity_id, component)

    def remove_component(self, entity_id, component):
        if entity_id in self.entities:
            for system in self.systems:
                system.remove_component(entity_id, component)
            return True
        return False

    def next_id(self):
        if not self.free_ids:
            self.highest_id += 1
            return self.highest_id
        return self.free_ids.popLeft()


    #### System Management ####
    ###########################
    def get_system(self, system_type):
        return self.systems[system_type]

    def add_system(self, system):
        old_system = self.systems.get(system.class_name, False)
        if old_system:
            old_system.destroy()
            self.systems[system.class_name] = system
        else:
            self.systems[system.class_name] = system
        if not isinstance(system, VoidSystem):
            for entity, components in self.entities.iteritems():
                system.add_entity(entity, components)
        system.prepare()
        return True

    def remove_system(self, system_type):
        system = self.systems.pop(system_type, None)
        if system:
            system.destroy()
            return True
        return False

    def tick(self):
        for system in self.systems:
            system.tick(previous_time)
