class Component(object):

    def __init__(self):
        self.entity_id = -1
        self.class_name = self.__class__.__name__

    @classmethod
    def cname(cls):
        return cls.__name__

    # Subclasses should store data here